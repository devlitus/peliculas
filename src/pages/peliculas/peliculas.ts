import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, InfiniteScroll } from 'ionic-angular';
import { TmdbProvider } from "../../providers/tmdb/tmdb";
import { ConfigProvider } from "../../providers/tmdb/config-image";

@IonicPage()
@Component({
  selector: 'page-peliculas',
  templateUrl: 'peliculas.html',
})
export class PeliculasPage {
  @ViewChild(InfiniteScroll) infiniteScroll: InfiniteScroll;
  public peliculas: Array<any> = [];
  public paginas: number = 0;
  public imagen: string = this.configImage.images.secure_base_url + this.configImage.images.backdrop_sizes[2];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _serviceTmdb: TmdbProvider,
    private configImage: ConfigProvider) {
  }

  ionViewDidLoad() {
    this.getPeliculas();
  }
  getPeliculas(){
    this.paginas = this.paginas + 1;
    this.infiniteScroll.waitFor(
      this._serviceTmdb.peliculas(this.paginas)
      .then(res => {
        Object.assign(this.peliculas,res);
        console.log(res);

        this.infiniteScroll.complete();
      })
      .catch(error =>{console.error('Fallo de conexion', error);})
      )
  }

}
